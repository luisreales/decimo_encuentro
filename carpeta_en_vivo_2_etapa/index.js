const chalk = require('chalk');
console.log('prueba acamica de nodemon');


const log = console.log;

//nombre azul y hobbie en amarillo

log(chalk.blue("Hola a todos soy NODEMON!, mi nombre es Luis") + chalk.yellow(" , me gusta jugar PS4 FIFa en especial!!!") );
// Compose multiple styles using the chainable API
log(chalk.blue.bgRed.bold('Hello world!'));

// Pass in multiple arguments
log(chalk.blue('Hello', 'World!', 'Foo', 'bar', 'biz', 'baz'));

// Nest styles
log(chalk.red('Hello', chalk.underline.bgBlue('world') + '!'));

// Nest styles of the same type even (color, underline, background)
log(chalk.green(
	'I am a green line ' +
	chalk.blue.underline.bold('with a blue substring') +
	' that becomes green again!'
));

//VARIABLES DE ENTORNO

var env = require("./env.environment.json");
//obtener el valor del enviroment configurado
console.log(env.development.SERVERURL);

//obtener el environment del servidor, configurar variable de entorno en la maquina

// var node_env = process.env.NODE_ENV;
// console.log('environment mi maquina',node_env) // dev

var node_env_local = process.env.NODE_ENV || 'development';

//sacar el valor del objeto

var variables = env[node_env_local];

console.log(`El puerto de desarrollo es: ${variables.PORT} paquete actualizado por el momento y su server url es ${variables.SERVERURL}`);




