const chalk = require('chalk');


//VARIABLES DE ENTORNO

var env = require("./env.environment.json");
//obtener el valor del enviroment configurado
console.log(env.development.SERVERURL);

//obtener el environment del servidor

// var node_env = process.env.NODE_ENV;
// console.log('environment mi maquina',node_env) // dev

var node_env_local = process.env.NODE_ENV || 'development';

//sacar el valor del objeto

var variables = env[node_env_local];

console.log(`El puerto de desarrollo es: ${variables.PORT} paquete actualizado por el momento y su server url es ${variables.SERVERURL}`);





const log = console.log;

//nombre azul y hobbie en amarillo

log(chalk.blue("Hola a todos soy NODEMON!, mi nombre es Luis") + chalk.yellow(" , me gusta jugar PS4 FIFa en especial!!!") );
// Compose multiple styles using the chainable API
log(chalk.blue.bgRed.bold('Hello world!'));

// Pass in multiple arguments
log(chalk.blue('Hello', 'World!', 'Foo', 'bar', 'biz', 'baz'));

// Nest styles
log(chalk.red('Hello', chalk.underline.bgBlue('world') + '!'));

// Nest styles of the same type even (color, underline, background)
log(chalk.green(
	'I am a green line ' +
	chalk.blue.underline.bold('with a blue substring') +
	' that becomes green again!'
));

// ES2015 template literal
log(`
CPU: ${chalk.red('90%')}
RAM: ${chalk.green('40%')}
DISK: ${chalk.yellow('70%')}
`);

var cpu = {
    totalPercent:50,
    used:15,
    total:80
}


var ram = {
    totalPercent:50,
    used:15,
    total:80
}


var disk = {
    totalPercent:50,
    used:15,
    total:80
}

// ES2015 tagged template literal
log(chalk`
CPU: {red ${cpu.totalPercent}%}
RAM: {green ${ram.used / ram.total * 100}%}
DISK: {rgb(255,131,0) ${disk.used / disk.total * 100}%}
`);

// Use RGB colors in terminal emulators that support it.
log(chalk.keyword('orange')('Yay for orange colored text!'));
log(chalk.rgb(123, 45, 67).underline('Underlined reddish color'));
log(chalk.hex('#DEADED').bold('Bold gray!'));